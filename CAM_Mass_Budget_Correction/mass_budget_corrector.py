import numpy as np
from scipy.sparse.linalg import factorized
from scipy.sparse import lil_matrix

class mass_budget_corrector(object):

    def __init__(self, lat, slat, slon, dtype=np.float32, dt=np.float32(21600.0)):

        self.dtype = dtype
        self.dt = self.dtype(dt)
        self.R = self.dtype(6.37e6)

        # Grid shape
        self.shape_chi = slat.size, slon.size
        self.size_chi = self.shape_chi[0] * self.shape_chi[1]
        self.shape_u = self.shape_chi
        self.shape_v = lat.size, slon.size

        # lat and lon in rad
        deg_to_rad = self.dtype(np.pi/180.0)
        self.sphi = deg_to_rad * slon.astype(self.dtype)
        self.slbda = deg_to_rad * slat.astype(self.dtype)
        self.lbda = deg_to_rad * lat.astype(self.dtype)

        self.d_phi = self.sphi[1] - self.sphi[0]
        self.d_lbda = self.slbda[1] - self.slbda[0]

        # Generate methods
        self.generate_solver()
        self.generate_div()
        self.generate_stag_interp()
        self.generate_grad_chi()

        # Init mass residual and stream function
        self.RHS = np.empty(self.shape_chi, dtype=self.dtype)
        self.chi = np.empty(self.shape_chi, dtype=self.dtype)
        self.grad_chi = (np.empty(self.shape_u, dtype=self.dtype),
                         np.empty(self.shape_v, dtype=self.dtype))


    def generate_solver(self):

        # Intermediate coefficients
        # -------------------------
        r = self.d_phi / self.d_lbda
        cos_lbda = np.cos(self.lbda)
        sin_lbda = np.sin(self.lbda)
        cos_slbda = np.cos(self.slbda)
        sin_slbda = np.sin(self.slbda)

        S_1 = 1.0 / (self.R**2 * self.d_phi * np.diff(sin_lbda))
        r_cos_slbda_1 = 1.0 / (r * cos_slbda)
        r_cos_lbda = r * cos_lbda

        # Matrix coefficients
        # -------------------
        A_0 = - S_1 * (2.0 * r_cos_slbda_1 + r_cos_lbda[1:] + r_cos_lbda[:-1])
        A_ip1 = S_1 * r_cos_slbda_1
        A_im1 = A_ip1
        A_jp1 = S_1 * r_cos_lbda[1:]
        A_jm1 = S_1 * r_cos_lbda[:-1]

        # Build sparse matrix
        # -------------------
        N, M = self.shape_chi
        A = lil_matrix((M*N, M*N), dtype=self.dtype)
        for j in range(N):
            for i in range(M):
                ij = j*M+i
                A[ij, ij] = A_0[j]
                A[ij, (j+1)*M-1 if i==0 else ij-1] = A_im1[j]
                A[ij, j*M if i==M-1 else ij+1] = A_ip1[j]
                if j > 0:
                    A[ij, ij-M] = A_jm1[j]
                if j < N-1:
                    A[ij, ij+M] = A_jp1[j]

        # Factorize matrix (prepare for inverting Laplacian)
        # --------------------------------------------------
        self.invert_lap = factorized(A.tocsc())


    def generate_div(self):

        # Coefficients for divergence computation
        # ---------------------------------------
        r_1 = self.d_lbda / self.d_phi
        cos_lbda = np.cos(self.lbda)
        S_1_div = 1.0 / (self.R * np.diff(np.sin(self.lbda)))
        N, M = self.shape_chi


        # Define divergence operator
        # --------------------------
        def div(u, v):

            F_u = r_1 * u[:,:]
            F_v = cos_lbda[1:-1, np.newaxis] * v[1:-1,:]

            div_U = np.zeros((N,M), dtype=self.dtype)
            div_U[:-1,:] += F_v
            div_U[1:,:] -= F_v
            div_U[:,:] += F_u
            div_U[:,1:] -= F_u[:,:-1]
            div_U[:,0] -= F_u[:,-1]

            div_U[:,:] *= S_1_div[:,np.newaxis]

            return div_U

        self.div = div


    def generate_stag_interp(self):

        # Coefficients for conservative interpolation (area weighted)
        # -----------------------------------------------------------
        sin_lbda = np.sin(self.lbda)
        sin_slbda = np.sin(self.slbda)
        diff_sin_lbda_1 = 1.0 / np.diff(sin_lbda)

        alpha_N = 0.5 * (sin_lbda[1:]-sin_slbda[:]) * diff_sin_lbda_1[:]
        alpha_S = 0.5 * (sin_lbda[:-1]-sin_slbda[:]) * diff_sin_lbda_1[:]


        # Define interpolation operator
        # -----------------------------
        def stag_interp(P):

            P_chi = np.empty(self.shape_chi, dtype=self.dtype)
            P_chi[:,1:] = alpha_N[:,np.newaxis] * (P[1:,:-1] + P[1:,1:]) \
                          + alpha_S[:,np.newaxis] * (P[:-1,:-1] + P[:-1,1:])
            P_chi[:,0] = alpha_N[:] * (P[1:,-1] + P[1:,0]) \
                         + alpha_S[:] * (P[:-1,-1] + P[:-1,0])

            return P_chi

        self.stag_interp = stag_interp


    def generate_grad_chi(self):

        # Coefficients for gradient computation
        # =====================================
        dl_lon = 1.0 / (self.R * np.cos(self.slbda) * self.d_phi)
        dl_lat = 1.0 / (self.R * self.d_lbda)
        cos_sphi = np.cos(self.sphi)
        sin_sphi = np.sin(self.sphi)
        coef = self.dtype(4.0 / (self.shape_chi[1] * self.R * self.d_lbda))


        # Define gradient operator
        # ========================
        def compute_grad_chi():

            grad_chi_lon = np.empty(self.shape_u, dtype=self.dtype)
            grad_chi_lat = np.empty(self.shape_v, dtype=self.dtype)

            # Zonal grad_chi
            # --------------
            grad_chi_lon[:,:-1] = dl_lon[:,np.newaxis] * (self.chi[:,1:]-self.chi[:,:-1])
            # Periodicity
            grad_chi_lon[:,-1] = dl_lon[:] * (self.chi[:,0]-self.chi[:,-1])

            # Meridional grad_chi
            # -------------------
            grad_chi_lat[1:-1] = dl_lat * (self.chi[1:,:]-self.chi[:-1,:])
            # south pole
            chi_cos = np.sum(self.chi[0,:]*cos_sphi)
            chi_sin = np.sum(self.chi[0,:]*sin_sphi)
            grad_chi_lat[0,:] = coef * (cos_sphi * chi_cos + sin_sphi * chi_sin)
            # north pole
            chi_cos = np.sum(self.chi[-1,:]*cos_sphi)
            chi_sin = np.sum(self.chi[-1,:]*sin_sphi)
            grad_chi_lat[-1,:] = - coef * (cos_sphi * chi_cos + sin_sphi * chi_sin)

            self.grad_chi[0][:] = grad_chi_lon[:]
            self.grad_chi[1][:] = grad_chi_lat[:]

        self.compute_grad_chi = compute_grad_chi


    def grad_chi_to_UV(self, PS, PT, Q_b, grad_chi=None, copy=True):

        if grad_chi is None:
            if copy:
                Uc, Vc = self.grad_chi[0].copy(), self.grad_chi[1].copy()
            else:
                Uc, Vc = self.grad_chi[0], self.grad_chi[1]
        else:
            if copy:
                Uc, Vc = grad_chi[0].copy(), grad_chi[1].copy()
            else:
                Uc, Vc = grad_chi[0], grad_chi[1]

        Uc[:,:] /= 0.5 * (PS[:-1,:] - Q_b[:-1,:] + PS[1:,:] - Q_b[1:,:]) - PT
        Vc[:,1:] /= 0.5 * (PS[:,:-1] - Q_b[:,:-1] + PS[:,1:] - Q_b[:,1:]) - PT
        Vc[:,0] /= 0.5 * (PS[:,-1] - Q_b[:,-1] + PS[:,0] - Q_b[:,0]) - PT

        if copy:
            return Uc, Vc


    def flux_correction(self, Uc, X_b, copy=True):

        X_b_u = np.empty(self.shape_u, self.dtype)
        X_b_v = np.empty(self.shape_v, self.dtype)
        X_b_u[:] = 0.5 * (X_b[:-1,:] + X_b[1:,:])
        X_b_v[:,1:] = 0.5 * (X_b[:,:-1] + X_b[:,1:])
        X_b_v[:,0] = 0.5 * (X_b[:,-1] + X_b[:,0])

        if copy:
            return Uc[0] * X_b_u, Uc[1] * X_b_v
        else:
            Uc[0][:] *= X_b_u[:]
            Uc[1][:] *= X_b_v[:]


    def __call__(self, PS, PT, Q_b, US_b, VS_b, ZMF, MMF,
                 PS_prv=None, PS_nxt=None, Q_b_prv=None, Q_b_nxt=None, dt=None):

        if dt is None:
            dt = self.dt
        else:
            dt = self.dtype(dt)

        # Check next and previous time steps
        if PS_nxt is None and PS_prv is None:
            raise ValueError("At least one of PS_prv or PS_nxt keyword argument must be given")
        if Q_b_nxt is None and Q_b_prv is None:
            raise ValueError("At least one of Q_b_prv or Q_b_nxt keyword argument must be given")
        if (PS_nxt is not None and Q_b_nxt is None) or (PS_nxt is None and Q_b_nxt is not None):
            raise ValueError("PS_nxt and Q_b_nxt keyword arguments must be simultaneously given or ignored")
        if (PS_prv is not None and Q_b_prv is None) or (PS_prv is None and Q_b_prv is not None):
            raise ValueError("PS_prv and Q_b_prv keyword arguments must be simultaneously given or ignored")

        # Cast input as dtype
        PS = PS.astype(self.dtype)
        PT = PT.astype(self.dtype)
        Q_b = Q_b.astype(self.dtype)
        US_b = US_b.astype(self.dtype)
        VS_b = VS_b.astype(self.dtype)
        ZMF = ZMF.astype(self.dtype)
        MMF = MMF.astype(self.dtype)
        if PS_nxt is not None:
            PS_nxt = PS_nxt.astype(self.dtype)
            Q_b_nxt = Q_b_nxt.astype(self.dtype)
        if PS_prv is not None:
            PS_prv = PS_prv.astype(self.dtype)
            Q_b_prv = Q_b_prv.astype(self.dtype)

        # Compute right hand side (mass residual)
        if PS_prv is None:
            dt_PS_Q_b = (PS_nxt - PS + Q_b_nxt - Q_b) / dt
        elif PS_nxt is None:
            dt_PS_Q_b = (PS - PS_prv + Q_b - Q_b_prv) / dt
        else:
            dt_PS_Q_b = (PS_nxt - PS_prv + Q_b_nxt - Q_b_prv) / (self.dtype(2.0) * dt)
        self.RHS[:,:] = self.stag_interp(dt_PS_Q_b) + self.div(US_b - ZMF, VS_b - MMF)

        # Invert laplacian
        self.chi[:,:] = self.invert_lap(self.RHS.flatten()).reshape(self.shape_chi)
        self.compute_grad_chi()
