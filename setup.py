from setuptools import setup, find_packages
from distutils.extension import Extension
import numpy as np

module1 = [Extension("PV_CAM_fv.DGrid_PV",
                     sources=["PV_CAM_fv/DGrid_PV.c"],
                     extra_compile_args=["-O3"],
                     include_dirs=[np.get_include(), "."])]

setup(name="CESM_tools",
      version="0.1",
      description="A set of post-processing tools specially designed for the CESM model",
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      url="https://git.iac.ethz.ch/leclairm/cesm_tools",
      install_requires=['netCDF4', 'numpy', 'scipy'],
      ext_modules=module1,
      entry_points={
          'console_scripts': ['cam_pv = PV_CAM_fv.console_scripts:compute_pv',
                              'cam_vapv = PV_CAM_fv.console_scripts:compute_vapv']
      },
      packages=['PV_CAM_fv', 'CAM_Mass_Budget_Correction'],
      package_data={'PV_CAM_fv': ['*.pxd', '*.pyx']}
)
