# ==================================================
#            Console scripts entry points
# ==================================================

from argparse import ArgumentParser, RawTextHelpFormatter
from . import DGrid_PV

def compute_pv():

    dsc = "Compute potential vorticity from raw D-Grid fv dycore CAM output"
    parser = ArgumentParser(description=dsc, formatter_class=RawTextHelpFormatter)

    parser.add_argument('ifile', help="Input file path")
    parser.add_argument('-o', '--ofile',
                        help="""Output file path. If none is given, write to input file.
If already existing and differs from input file, overwrite""")
    help_str = """time monitoring verbosity:
<0 no monitoring
 1 total time spent and progress
>1 detailed time in init, read, write and process phases"""
    parser.add_argument('-m', '--monitor_level', required=False,
                        help=help_str, type=int, default=1)
    parser.add_argument('--n_start', type=int, default=None,
                        help="first time step to treat")
    parser.add_argument('--n_end', type=int, default=None,
                        help="last time step to treat")

    args = parser.parse_args()

    DGrid_PV.compute_PV_from_file(args.ifile, out_file=args.ofile,
                                  n_start=args.n_start, n_end=args.n_end,
                                  monitor_level=args.monitor_level)

def compute_vapv():

    dsc = """Compute vertically averaged potential vorticity
from raw D-Grid fv dycore CAM output"""
    parser = ArgumentParser(description=dsc, formatter_class=RawTextHelpFormatter)

    parser.add_argument('ifile', help="Input file path")
    parser.add_argument('-o', '--ofile', required=False,
                        help="""Output file path. If none is given, write to input file.
If already existing and differs from input file, overwrite""")
    parser.add_argument('--pmin', required=True, type=float,
                        help="pressure interval lower boundary [Pa] (required)")
    parser.add_argument('--pmax', required=True, type=float,
                        help="pressure interval upper boundary [Pa] (required)")
    help_str = """time monitoring verbosity:
<0 no monitoring
 1 total time spent and progress
>1 detailed time in init, read, write and process phases"""
    parser.add_argument('-m', '--monitor_level', required=False,
                        help=help_str, type=int, default=1)
    parser.add_argument('--n_start', type=int, default=None,
                        help="first time step to treat")
    parser.add_argument('--n_end', type=int, default=None,
                        help="last time step to treat")

    args = parser.parse_args()

    DGrid_PV.compute_PV_from_file(args.ifile, out_file=args.ofile, v_avg=True,
                                  P_min=args.pmin, P_max=args.pmax,
                                  n_start=args.n_start, n_end=args.n_end,
                                  monitor_level=args.monitor_level)
